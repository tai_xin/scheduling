import {
  dept,
  leaveType,
  tempShifts,
  employeeVo,
  restTime,
  year,
  group,
  attendance,
  Class,
} from '@/const/api'

export default {
  methods: {
    // 获取部门列表
    async getDepartmentList(id) {
      let {data, state, code, msg, count} = await this.$axios.$get(
        dept('getList'),
        {params: {id: id}},
      )
      if (state == this.ResCode) {
        data &&
          data.length &&
          data.forEach((n1, i1) => {
            n1.label = n1.deptName
            n1.value = n1.id
          })
        this.mask1
          ? (this.mask1.deptIdOptions = data)
          : (this.deptIdOptions = data)
        this.mask2
          ? (this.mask2.deptIdOptions = data)
          : (this.deptIdOptions = data)
        this.mask3
          ? (this.mask3.deptIdOptions = data)
          : (this.deptIdOptions = data)
        this.mask4
          ? (this.mask4.deptIdOptions = data)
          : (this.deptIdOptions = data)
        this.mask
          ? (this.mask.deptIdOptions = data)
          : (this.deptIdOptions = data)
        this.deptIdOptions = data
      }
    },
    // 获取事由类型列表
    async getLeaveTypeList(id) {
      let {data, state, code, msg, count} = await this.$axios.$get(
        leaveType('getLeaveTypeList'),
        {params: {id: id}},
      )
      if (state == this.ResCode) {
        data &&
          data.length &&
          data.forEach((n1, i1) => {
            n1.label = n1.typeName
            n1.value = n1.id
          })
        this.mask1
          ? (this.mask1.leaveTypeOptions = data)
          : (this.leaveTypeOptions = data)
        this.mask2
          ? (this.mask2.leaveTypeOptions = data)
          : (this.leaveTypeOptions = data)
        this.mask3
          ? (this.mask3.leaveTypeOptions = data)
          : (this.leaveTypeOptions = data)
        this.mask4
          ? (this.mask4.leaveTypeOptions = data)
          : (this.leaveTypeOptions = data)
        this.leaveTypeOptions = data
      }
    },
    // 获取班次列表
    async getShiftsList(id) {
      let {data, state, code, msg, count} = await this.$axios.$get(
        tempShifts('List'),
        {params: {deptId: id, pageNum: 0, pageSize: 0}},
      )
      if (state == this.ResCode) {
        data &&
          data.length &&
          data.forEach((n1, i1) => {
            n1.label = n1.className
            n1.value = n1.id
          })
        this.mask1
          ? (this.mask1.shiftsOptions = data)
          : (this.shiftsOptions = data)
        this.mask2
          ? (this.mask2.shiftsOptions = data)
          : (this.shiftsOptions = data)
        this.mask3
          ? (this.mask3.shiftsOptions = data)
          : (this.shiftsOptions = data)
        this.mask4
          ? (this.mask4.shiftsOptions = data)
          : (this.shiftsOptions = data)
        this.mask
          ? (this.mask.shiftsOptions = data)
          : (this.shiftsOptions = data)
        this.shiftsOptions = data
        this.OriginShiftsOptions = data
      }
    },
    // 获取所有班次列表 潘
    async getAllShiftsList() {
      let {data, state, code, msg, count} = await this.$axios.$get(
        Class('selectAllClass'),
      )
      if (state == this.ResCode) {
        data &&
          data.length &&
          data.forEach((n1, i1) => {
            n1.label = n1.className
            n1.value = n1.id
          })
        this.mask1
          ? (this.mask1.shiftsOptions = data)
          : (this.shiftsOptions = data)
        this.mask2
          ? (this.mask2.shiftsOptions = data)
          : (this.shiftsOptions = data)
        this.mask3
          ? (this.mask3.shiftsOptions = data)
          : (this.shiftsOptions = data)
        this.mask4
          ? (this.mask4.shiftsOptions = data)
          : (this.shiftsOptions = data)
        this.mask
          ? (this.mask.shiftsOptions = data)
          : (this.shiftsOptions = data)
        this.shiftsOptions = data
        this.OriginShiftsOptions = data
      }
    },
    // 获取角色列表
    async getEmployeeVo(a, b) {
      let {data, state, code, msg, count} = await this.$axios.$get(
        employeeVo('getList'),
        {params: {deptId: a, roleId: b}},
      )
      if (state == this.ResCode) {
        console.log(data, 'getEmployeeVo')
        data &&
          data.length &&
          data.forEach((n1, i1) => {
            n1.label = n1.employeeName
            n1.value = n1.employeeNo || n1.wokerNo || n1.id
          })
        if (b == 2) {
          // 审批 审核
          this.approvalOptions = data
          this.checkOptions = data
          this.CCOptions = data
        } else if (b == 1) {
          // 普通员工
          this.CCOptions = data
          this.AllOption = data
        } else {
          // 没roleId
          this.AllOption = data
        }
      }
    },
    // 获取剩余假期(小时)
    async getRestTime(a) {
      let {data, state, code, msg, count} = await this.$axios.$get(
        restTime('getRestTime'),
        {params: {wokerNo: a}},
      )
      if (state == this.ResCode) {
        this.restTime = data
        this.restTime = data
        this.restTime = data
        this.restTime = data
      }
    },
    // 获取角色列表
    async getRoles() {
      let {data, state, code, msg, count} = await this.$axios.$get(
        group('selectRole'),
      )
      if (state == this.ResCode) {
        data &&
          data.length &&
          data.forEach((n1, i1) => {
            n1.label = n1.name
            n1.value = n1.id
          })
        this.mask
          ? (this.mask.roleIdOptions = data)
          : (this.roleIdOptions = data)
      }
    },
    // 获取假期列表
    async getYear(a) {
      let {data, state, code, msg, count} = await this.$axios.$get(
        year('getHoliday'),
        {params: {year: a}},
      )
      if (state == this.ResCode) {
        this.holidayList = data
      }
    },
    // 获取所有部门 以及 下面的员工
    async getAllWorker() {
      let {data, state, code, msg, count} = await this.$axios.$get(
        attendance('selectEmployee'),
      )
      if (state == this.ResCode) {
        data &&
          data.length &&
          data.forEach((n1, i1) => {
            n1.label = n1.deptName
            n1.value = n1.id
            n1.employeeVoList &&
              n1.employeeVoList.length &&
              ((n1.children = JSON.parse(JSON.stringify(n1.employeeVoList))),
              n1.children.forEach((n2, i2) => {
                n2.label = n2.employeeName
                n2.value = n2.employeeNo
              }))
          })
        this.allWorker = data
      }
    },
    // 获取剩余假期时长
    FnTypeChange(e) {
      let str = ''
      if (this.restTime && this.restTime.length) {
        switch (e) {
          case '4':
            str = '年假剩余假期' + this.restTime[0].annualmaining + '小时'
            break
          case '5':
            str = '节假剩余假期' + this.restTime[0].holidaymaining + '小时'
            break
          case '6':
            str = ''
            break
          default:
            str = '剩余休息假期' + this.restTime[0].holidaymaining + '小时'
            break
        }
      } else {
        str = '没有剩余假期'
      }

      this.mask1 && (this.mask1.data.restTimeStr = str)
      this.mask2 && (this.mask2.data.restTimeStr = str)
      this.mask3 && (this.mask3.data.restTimeStr = str)
      this.mask4 && (this.mask4.data.restTimeStr = str)
    },
  },
}
