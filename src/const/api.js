const apiVersion = '/api/v1'
const deepexiDashboard = `/deepexi-dashboard${apiVersion}`
const enterpriseContact = `/xpaas-enterprise-contact${apiVersion}`
const consoleApi = `/xpaas-console-api${apiVersion}`

// 根据thirdId获取头部导航栏列表
export const getXpaasTag = thirdId =>
  `${deepexiDashboard}/tenantComponents/xpaas/getXpaasTag/${thirdId}`

// 用户名密码登录
export const loginByUsername = `/user/login`

// 获取头部导航栏thirdId
export const adminUser = `${enterpriseContact}/users/adminUser`

// 根据appId获取左侧菜单
export const userMenuTree = appId =>
  `${consoleApi}/xpassPermission/userMenuTree/${appId}`

// 班次管理系列接口
export const tempShifts = prefix => `/WorkShifts/${prefix}`

// 部门接口
export const dept = prefix => `/dept/${prefix}`

// 事由类型
export const leaveType = prefix => `/leaveType/${prefix}`

// 调休管理 调休换班
export const holiday = prefix => `/HolidayApply/${prefix}`

// 获取角色列表
export const employeeVo = prefix => `/employeeVo/${prefix}`

// 获取剩余假期(小时)
export const restTime = prefix => `/rest/${prefix}`

// 获取假期列表
export const year = prefix => `/getYear/${prefix}`

// 临时下班申请
export const tmpApply = prefix => `/TemporaryWorkApply/${prefix}`

// 节假日管理
export const holidayRegulato = prefix => `/HolidayRegulato/${prefix}`

// 班组管理
export const group = prefix => `/Group/${prefix}`

// 部门下的员工
export const attendance = prefix => `/attendance/${prefix}`

// 排班管理
export const Class = prefix => `/class/${prefix}`
